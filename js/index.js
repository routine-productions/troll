$(document).ready(function () {

    $("audio").each(function(){
        $(this).bind("play",stopAll);
    });

    function stopAll(e){
        var currentElementId=$(e.currentTarget).attr("id");
        $("audio").each(function(){
            var $this=$(this);
            var elementId=$this.attr("id");
            if(elementId!=currentElementId){
                $this[0].pause();
            }
        });
    }

    //$('input[type="text"],textarea').placeholder({animate: 'fade'});

});
$(document).ready(function(){

    $(".placeholderanimation").each(function(){
        $(this).on("keyup",function(){
            if($(this).val()){
                $(this).addClass("active");
            }else{
                $(this).removeClass("active");
            }
        });
    });

});