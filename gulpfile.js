//var gulp = require('gulp');
var elixir = require('laravel-elixir');
//var csscomb = require('gulp-csscomb');

elixir.config.publicPath = '';
elixir.config.assetsPath = '';
elixir.config.css.sass.folder = 'scss';


elixir(function (mix) {
    mix.sass('index.scss');
});

//gulp.task('styles', function () {
//    return gulp.src('scss/**/*')
//        .pipe(csscomb())
//        .pipe(gulp.dest('./build/css'));
//});
