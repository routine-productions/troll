(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var Eye, Toy;

Eye = require('./lib/eye');

Toy = require('./lib/toy');

document.addEventListener('DOMContentLoaded', function() {
  var i, index, len, results, toy, toys;
  new Eye('.fey-part.__eye-container-left', -0.7, 0.7, -0.3, 0.4);
  new Eye('.fey-part.__eye-container-right', -0.4, 0.5, -0.2, 0.4);
  toys = document.querySelectorAll('.fey-toy');
  results = [];
  for (index = i = 0, len = toys.length; i < len; index = ++i) {
    toy = toys[index];
    results.push(new Toy(toy, index));
  }
  return results;
});


},{"./lib/eye":2,"./lib/toy":3}],2:[function(require,module,exports){
var Eye,
  bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

module.exports = Eye = (function() {
  function Eye(selector, minX1, maxX1, minY1, maxY1) {
    this.minX = minX1;
    this.maxX = maxX1;
    this.minY = minY1;
    this.maxY = maxY1;
    this.onMouseMove = bind(this.onMouseMove, this);
    this.node = document.querySelector(selector);
    this.eye = this.node.querySelector('.__eye');
    this.x = 0;
    this.y = 0;
    this.mouseX = document.documentElement.clientWidth / 2;
    this.mouseY = document.documentElement.clientHeight / 2;
    this.initHandlers();
    this.render();
    return;
  }

  Eye.prototype.initHandlers = function() {
    window.addEventListener('mousemove', this.onMouseMove);
  };

  Eye.prototype.toPx = function(em) {
    return em * parseFloat(getComputedStyle(this.node).fontSize);
  };

  Eye.prototype.toEm = function(px) {
    return px / parseFloat(getComputedStyle(this.node).fontSize);
  };

  Eye.prototype.getPosition = function() {
    var rect, x, y;
    rect = this.node.getBoundingClientRect();
    x = rect.left - (rect.width / 2);
    y = rect.top - (rect.height / 2);
    return {
      x: x,
      y: y
    };
  };

  Eye.prototype.constraint = function() {
    var maxX, maxY, minX, minY;
    minX = this.toPx(this.minX);
    maxX = this.toPx(this.maxX);
    minY = this.toPx(this.minY);
    maxY = this.toPx(this.maxY);
    if (this.x < minX) {
      this.x = minX;
    }
    if (this.x > maxX) {
      this.x = maxX;
    }
    if (this.y < minY) {
      this.y = minY;
    }
    if (this.y > maxY) {
      this.y = maxY;
    }
  };

  Eye.prototype.calcEyes = function() {
    var clientHeight, clientWidth, h, mouseX, mouseY, position, q, w;
    position = this.getPosition();
    clientWidth = document.documentElement.clientWidth;
    clientHeight = document.documentElement.clientHeight;
    if (this.mouseX < position.x) {
      q = (this.mouseX / position.x) - 1;
      this.x = this.toPx(this.minX) * -q;
    } else {
      w = clientWidth - position.x;
      mouseX = clientWidth - this.mouseX;
      q = (mouseX / w) - 1;
      this.x = this.toPx(this.maxX) * -q;
    }
    if (this.mouseY < position.y) {
      q = (this.mouseY / position.y) - 1;
      this.y = this.toPx(this.minY) * -q;
    } else {
      h = clientHeight - position.y;
      mouseY = clientHeight - this.mouseY;
      q = (mouseY / h) - 1;
      this.y = this.toPx(this.maxY) * -q;
    }
    this.constraint();
  };

  Eye.prototype.render = function() {
    this.calcEyes();
    this.eye.style.transform = "translate(" + this.x + "px, " + this.y + "px) rotate3d(0,0,0,0)";
    requestAnimationFrame((function(_this) {
      return function() {
        return _this.render();
      };
    })(this));
  };

  Eye.prototype.onMouseMove = function(event) {
    this.mouseX = event.clientX;
    this.mouseY = event.clientY;
  };

  return Eye;

})();


},{}],3:[function(require,module,exports){
var Toy,
  bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

module.exports = Toy = (function() {
  Toy.audios = [
    {
      tone: 'audio/tone1.mp3',
      theme: 'audio/theme1.mp3'
    }, {
      tone: 'audio/tone2.mp3',
      theme: 'audio/theme2.mp3'
    }, {
      tone: 'audio/tone3.mp3',
      theme: 'audio/theme3.mp3'
    }, {
      tone: 'audio/tone4.mp3',
      theme: 'audio/theme4.mp3'
    }, {
      tone: 'audio/tone5.mp3',
      theme: 'audio/theme5.mp3'
    }, {
      tone: 'audio/tone6.mp3',
      theme: 'audio/theme6.mp3'
    }
  ];

  Toy.toys = [];

  Toy.stopAllThemes = function() {
    var i, len, ref, toy;
    ref = Toy.toys;
    for (i = 0, len = ref.length; i < len; i++) {
      toy = ref[i];
      toy.stopTheme();
    }
  };

  function Toy(node, index) {
    this.node = node;
    this.index = index;
    this.onClick = bind(this.onClick, this);
    this.onMouseOver = bind(this.onMouseOver, this);
    Toy.toys.push(this);
    this.src = Toy.audios[this.index];
    this.initHandlers();
    return;
  }

  Toy.prototype.initHandlers = function() {
    this.node.addEventListener('mouseover', this.onMouseOver);
    this.node.addEventListener('click', this.onClick);
  };

  Toy.prototype.onMouseOver = function() {
    this.playTone();
    dynamics.animate(this.node, {
      rotateZ: 90
    }, {
      type: dynamics.bounce,
      duration: 3000,
      frequency: 1200,
      friction: 0.5
    });
  };

  Toy.prototype.onClick = function() {
    this.playTheme();
  };

  Toy.prototype.playTone = function() {
    this.playAudio(this.src.tone, 0.25);
  };

  Toy.prototype.playTheme = function() {
    Toy.stopAllThemes();
    this.theme = this.playAudio(this.src.theme, 0.5);
  };

  Toy.prototype.stopTheme = function() {
    var ref;
    if ((ref = this.theme) != null) {
      ref.pause();
    }
  };

  Toy.prototype.playAudio = function(src, volume) {
    var audio;
    if (volume == null) {
      volume = 0.5;
    }
    audio = new Audio();
    audio.volume = volume;
    audio.src = src;
    audio.autoplay = true;
    return audio;
  };

  return Toy;

})();


},{}]},{},[1])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImM6XFxsb2NhbGhvc3RcXGZleVxcbm9kZV9tb2R1bGVzXFxndWxwLWJyb3dzZXJpZnlcXG5vZGVfbW9kdWxlc1xcYnJvd3NlcmlmeVxcbm9kZV9tb2R1bGVzXFxicm93c2VyLXBhY2tcXF9wcmVsdWRlLmpzIiwiYzovbG9jYWxob3N0L2ZleS9zcmMvaW5kZXguY29mZmVlIiwiYzovbG9jYWxob3N0L2ZleS9zcmMvbGliL2V5ZS5jb2ZmZWUiLCJjOi9sb2NhbGhvc3QvZmV5L3NyYy9saWIvdG95LmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbkJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDN0dBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt0aHJvdyBuZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpfXZhciBmPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChmLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGYsZi5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJ2YXIgRXllLCBUb3k7XG5cbkV5ZSA9IHJlcXVpcmUoJy4vbGliL2V5ZScpO1xuXG5Ub3kgPSByZXF1aXJlKCcuL2xpYi90b3knKTtcblxuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIGZ1bmN0aW9uKCkge1xuICB2YXIgaSwgaW5kZXgsIGxlbiwgcmVzdWx0cywgdG95LCB0b3lzO1xuICBuZXcgRXllKCcuZmV5LXBhcnQuX19leWUtY29udGFpbmVyLWxlZnQnLCAtMC43LCAwLjcsIC0wLjMsIDAuNCk7XG4gIG5ldyBFeWUoJy5mZXktcGFydC5fX2V5ZS1jb250YWluZXItcmlnaHQnLCAtMC40LCAwLjUsIC0wLjIsIDAuNCk7XG4gIHRveXMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuZmV5LXRveScpO1xuICByZXN1bHRzID0gW107XG4gIGZvciAoaW5kZXggPSBpID0gMCwgbGVuID0gdG95cy5sZW5ndGg7IGkgPCBsZW47IGluZGV4ID0gKytpKSB7XG4gICAgdG95ID0gdG95c1tpbmRleF07XG4gICAgcmVzdWx0cy5wdXNoKG5ldyBUb3kodG95LCBpbmRleCkpO1xuICB9XG4gIHJldHVybiByZXN1bHRzO1xufSk7XG5cbiIsInZhciBFeWUsXG4gIGJpbmQgPSBmdW5jdGlvbihmbiwgbWUpeyByZXR1cm4gZnVuY3Rpb24oKXsgcmV0dXJuIGZuLmFwcGx5KG1lLCBhcmd1bWVudHMpOyB9OyB9O1xuXG5tb2R1bGUuZXhwb3J0cyA9IEV5ZSA9IChmdW5jdGlvbigpIHtcbiAgZnVuY3Rpb24gRXllKHNlbGVjdG9yLCBtaW5YMSwgbWF4WDEsIG1pblkxLCBtYXhZMSkge1xuICAgIHRoaXMubWluWCA9IG1pblgxO1xuICAgIHRoaXMubWF4WCA9IG1heFgxO1xuICAgIHRoaXMubWluWSA9IG1pblkxO1xuICAgIHRoaXMubWF4WSA9IG1heFkxO1xuICAgIHRoaXMub25Nb3VzZU1vdmUgPSBiaW5kKHRoaXMub25Nb3VzZU1vdmUsIHRoaXMpO1xuICAgIHRoaXMubm9kZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3Ioc2VsZWN0b3IpO1xuICAgIHRoaXMuZXllID0gdGhpcy5ub2RlLnF1ZXJ5U2VsZWN0b3IoJy5fX2V5ZScpO1xuICAgIHRoaXMueCA9IDA7XG4gICAgdGhpcy55ID0gMDtcbiAgICB0aGlzLm1vdXNlWCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRXaWR0aCAvIDI7XG4gICAgdGhpcy5tb3VzZVkgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50SGVpZ2h0IC8gMjtcbiAgICB0aGlzLmluaXRIYW5kbGVycygpO1xuICAgIHRoaXMucmVuZGVyKCk7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgRXllLnByb3RvdHlwZS5pbml0SGFuZGxlcnMgPSBmdW5jdGlvbigpIHtcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignbW91c2Vtb3ZlJywgdGhpcy5vbk1vdXNlTW92ZSk7XG4gIH07XG5cbiAgRXllLnByb3RvdHlwZS50b1B4ID0gZnVuY3Rpb24oZW0pIHtcbiAgICByZXR1cm4gZW0gKiBwYXJzZUZsb2F0KGdldENvbXB1dGVkU3R5bGUodGhpcy5ub2RlKS5mb250U2l6ZSk7XG4gIH07XG5cbiAgRXllLnByb3RvdHlwZS50b0VtID0gZnVuY3Rpb24ocHgpIHtcbiAgICByZXR1cm4gcHggLyBwYXJzZUZsb2F0KGdldENvbXB1dGVkU3R5bGUodGhpcy5ub2RlKS5mb250U2l6ZSk7XG4gIH07XG5cbiAgRXllLnByb3RvdHlwZS5nZXRQb3NpdGlvbiA9IGZ1bmN0aW9uKCkge1xuICAgIHZhciByZWN0LCB4LCB5O1xuICAgIHJlY3QgPSB0aGlzLm5vZGUuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gICAgeCA9IHJlY3QubGVmdCAtIChyZWN0LndpZHRoIC8gMik7XG4gICAgeSA9IHJlY3QudG9wIC0gKHJlY3QuaGVpZ2h0IC8gMik7XG4gICAgcmV0dXJuIHtcbiAgICAgIHg6IHgsXG4gICAgICB5OiB5XG4gICAgfTtcbiAgfTtcblxuICBFeWUucHJvdG90eXBlLmNvbnN0cmFpbnQgPSBmdW5jdGlvbigpIHtcbiAgICB2YXIgbWF4WCwgbWF4WSwgbWluWCwgbWluWTtcbiAgICBtaW5YID0gdGhpcy50b1B4KHRoaXMubWluWCk7XG4gICAgbWF4WCA9IHRoaXMudG9QeCh0aGlzLm1heFgpO1xuICAgIG1pblkgPSB0aGlzLnRvUHgodGhpcy5taW5ZKTtcbiAgICBtYXhZID0gdGhpcy50b1B4KHRoaXMubWF4WSk7XG4gICAgaWYgKHRoaXMueCA8IG1pblgpIHtcbiAgICAgIHRoaXMueCA9IG1pblg7XG4gICAgfVxuICAgIGlmICh0aGlzLnggPiBtYXhYKSB7XG4gICAgICB0aGlzLnggPSBtYXhYO1xuICAgIH1cbiAgICBpZiAodGhpcy55IDwgbWluWSkge1xuICAgICAgdGhpcy55ID0gbWluWTtcbiAgICB9XG4gICAgaWYgKHRoaXMueSA+IG1heFkpIHtcbiAgICAgIHRoaXMueSA9IG1heFk7XG4gICAgfVxuICB9O1xuXG4gIEV5ZS5wcm90b3R5cGUuY2FsY0V5ZXMgPSBmdW5jdGlvbigpIHtcbiAgICB2YXIgY2xpZW50SGVpZ2h0LCBjbGllbnRXaWR0aCwgaCwgbW91c2VYLCBtb3VzZVksIHBvc2l0aW9uLCBxLCB3O1xuICAgIHBvc2l0aW9uID0gdGhpcy5nZXRQb3NpdGlvbigpO1xuICAgIGNsaWVudFdpZHRoID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudFdpZHRoO1xuICAgIGNsaWVudEhlaWdodCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRIZWlnaHQ7XG4gICAgaWYgKHRoaXMubW91c2VYIDwgcG9zaXRpb24ueCkge1xuICAgICAgcSA9ICh0aGlzLm1vdXNlWCAvIHBvc2l0aW9uLngpIC0gMTtcbiAgICAgIHRoaXMueCA9IHRoaXMudG9QeCh0aGlzLm1pblgpICogLXE7XG4gICAgfSBlbHNlIHtcbiAgICAgIHcgPSBjbGllbnRXaWR0aCAtIHBvc2l0aW9uLng7XG4gICAgICBtb3VzZVggPSBjbGllbnRXaWR0aCAtIHRoaXMubW91c2VYO1xuICAgICAgcSA9IChtb3VzZVggLyB3KSAtIDE7XG4gICAgICB0aGlzLnggPSB0aGlzLnRvUHgodGhpcy5tYXhYKSAqIC1xO1xuICAgIH1cbiAgICBpZiAodGhpcy5tb3VzZVkgPCBwb3NpdGlvbi55KSB7XG4gICAgICBxID0gKHRoaXMubW91c2VZIC8gcG9zaXRpb24ueSkgLSAxO1xuICAgICAgdGhpcy55ID0gdGhpcy50b1B4KHRoaXMubWluWSkgKiAtcTtcbiAgICB9IGVsc2Uge1xuICAgICAgaCA9IGNsaWVudEhlaWdodCAtIHBvc2l0aW9uLnk7XG4gICAgICBtb3VzZVkgPSBjbGllbnRIZWlnaHQgLSB0aGlzLm1vdXNlWTtcbiAgICAgIHEgPSAobW91c2VZIC8gaCkgLSAxO1xuICAgICAgdGhpcy55ID0gdGhpcy50b1B4KHRoaXMubWF4WSkgKiAtcTtcbiAgICB9XG4gICAgdGhpcy5jb25zdHJhaW50KCk7XG4gIH07XG5cbiAgRXllLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbigpIHtcbiAgICB0aGlzLmNhbGNFeWVzKCk7XG4gICAgdGhpcy5leWUuc3R5bGUudHJhbnNmb3JtID0gXCJ0cmFuc2xhdGUoXCIgKyB0aGlzLnggKyBcInB4LCBcIiArIHRoaXMueSArIFwicHgpIHJvdGF0ZTNkKDAsMCwwLDApXCI7XG4gICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKChmdW5jdGlvbihfdGhpcykge1xuICAgICAgcmV0dXJuIGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4gX3RoaXMucmVuZGVyKCk7XG4gICAgICB9O1xuICAgIH0pKHRoaXMpKTtcbiAgfTtcblxuICBFeWUucHJvdG90eXBlLm9uTW91c2VNb3ZlID0gZnVuY3Rpb24oZXZlbnQpIHtcbiAgICB0aGlzLm1vdXNlWCA9IGV2ZW50LmNsaWVudFg7XG4gICAgdGhpcy5tb3VzZVkgPSBldmVudC5jbGllbnRZO1xuICB9O1xuXG4gIHJldHVybiBFeWU7XG5cbn0pKCk7XG5cbiIsInZhciBUb3ksXG4gIGJpbmQgPSBmdW5jdGlvbihmbiwgbWUpeyByZXR1cm4gZnVuY3Rpb24oKXsgcmV0dXJuIGZuLmFwcGx5KG1lLCBhcmd1bWVudHMpOyB9OyB9O1xuXG5tb2R1bGUuZXhwb3J0cyA9IFRveSA9IChmdW5jdGlvbigpIHtcbiAgVG95LmF1ZGlvcyA9IFtcbiAgICB7XG4gICAgICB0b25lOiAnYXVkaW8vdG9uZTEubXAzJyxcbiAgICAgIHRoZW1lOiAnYXVkaW8vdGhlbWUxLm1wMydcbiAgICB9LCB7XG4gICAgICB0b25lOiAnYXVkaW8vdG9uZTIubXAzJyxcbiAgICAgIHRoZW1lOiAnYXVkaW8vdGhlbWUyLm1wMydcbiAgICB9LCB7XG4gICAgICB0b25lOiAnYXVkaW8vdG9uZTMubXAzJyxcbiAgICAgIHRoZW1lOiAnYXVkaW8vdGhlbWUzLm1wMydcbiAgICB9LCB7XG4gICAgICB0b25lOiAnYXVkaW8vdG9uZTQubXAzJyxcbiAgICAgIHRoZW1lOiAnYXVkaW8vdGhlbWU0Lm1wMydcbiAgICB9LCB7XG4gICAgICB0b25lOiAnYXVkaW8vdG9uZTUubXAzJyxcbiAgICAgIHRoZW1lOiAnYXVkaW8vdGhlbWU1Lm1wMydcbiAgICB9LCB7XG4gICAgICB0b25lOiAnYXVkaW8vdG9uZTYubXAzJyxcbiAgICAgIHRoZW1lOiAnYXVkaW8vdGhlbWU2Lm1wMydcbiAgICB9XG4gIF07XG5cbiAgVG95LnRveXMgPSBbXTtcblxuICBUb3kuc3RvcEFsbFRoZW1lcyA9IGZ1bmN0aW9uKCkge1xuICAgIHZhciBpLCBsZW4sIHJlZiwgdG95O1xuICAgIHJlZiA9IFRveS50b3lzO1xuICAgIGZvciAoaSA9IDAsIGxlbiA9IHJlZi5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgdG95ID0gcmVmW2ldO1xuICAgICAgdG95LnN0b3BUaGVtZSgpO1xuICAgIH1cbiAgfTtcblxuICBmdW5jdGlvbiBUb3kobm9kZSwgaW5kZXgpIHtcbiAgICB0aGlzLm5vZGUgPSBub2RlO1xuICAgIHRoaXMuaW5kZXggPSBpbmRleDtcbiAgICB0aGlzLm9uQ2xpY2sgPSBiaW5kKHRoaXMub25DbGljaywgdGhpcyk7XG4gICAgdGhpcy5vbk1vdXNlT3ZlciA9IGJpbmQodGhpcy5vbk1vdXNlT3ZlciwgdGhpcyk7XG4gICAgVG95LnRveXMucHVzaCh0aGlzKTtcbiAgICB0aGlzLnNyYyA9IFRveS5hdWRpb3NbdGhpcy5pbmRleF07XG4gICAgdGhpcy5pbml0SGFuZGxlcnMoKTtcbiAgICByZXR1cm47XG4gIH1cblxuICBUb3kucHJvdG90eXBlLmluaXRIYW5kbGVycyA9IGZ1bmN0aW9uKCkge1xuICAgIHRoaXMubm9kZS5hZGRFdmVudExpc3RlbmVyKCdtb3VzZW92ZXInLCB0aGlzLm9uTW91c2VPdmVyKTtcbiAgICB0aGlzLm5vZGUuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCB0aGlzLm9uQ2xpY2spO1xuICB9O1xuXG4gIFRveS5wcm90b3R5cGUub25Nb3VzZU92ZXIgPSBmdW5jdGlvbigpIHtcbiAgICB0aGlzLnBsYXlUb25lKCk7XG4gICAgZHluYW1pY3MuYW5pbWF0ZSh0aGlzLm5vZGUsIHtcbiAgICAgIHJvdGF0ZVo6IDkwXG4gICAgfSwge1xuICAgICAgdHlwZTogZHluYW1pY3MuYm91bmNlLFxuICAgICAgZHVyYXRpb246IDMwMDAsXG4gICAgICBmcmVxdWVuY3k6IDEyMDAsXG4gICAgICBmcmljdGlvbjogMC41XG4gICAgfSk7XG4gIH07XG5cbiAgVG95LnByb3RvdHlwZS5vbkNsaWNrID0gZnVuY3Rpb24oKSB7XG4gICAgdGhpcy5wbGF5VGhlbWUoKTtcbiAgfTtcblxuICBUb3kucHJvdG90eXBlLnBsYXlUb25lID0gZnVuY3Rpb24oKSB7XG4gICAgdGhpcy5wbGF5QXVkaW8odGhpcy5zcmMudG9uZSwgMC4yNSk7XG4gIH07XG5cbiAgVG95LnByb3RvdHlwZS5wbGF5VGhlbWUgPSBmdW5jdGlvbigpIHtcbiAgICBUb3kuc3RvcEFsbFRoZW1lcygpO1xuICAgIHRoaXMudGhlbWUgPSB0aGlzLnBsYXlBdWRpbyh0aGlzLnNyYy50aGVtZSwgMC41KTtcbiAgfTtcblxuICBUb3kucHJvdG90eXBlLnN0b3BUaGVtZSA9IGZ1bmN0aW9uKCkge1xuICAgIHZhciByZWY7XG4gICAgaWYgKChyZWYgPSB0aGlzLnRoZW1lKSAhPSBudWxsKSB7XG4gICAgICByZWYucGF1c2UoKTtcbiAgICB9XG4gIH07XG5cbiAgVG95LnByb3RvdHlwZS5wbGF5QXVkaW8gPSBmdW5jdGlvbihzcmMsIHZvbHVtZSkge1xuICAgIHZhciBhdWRpbztcbiAgICBpZiAodm9sdW1lID09IG51bGwpIHtcbiAgICAgIHZvbHVtZSA9IDAuNTtcbiAgICB9XG4gICAgYXVkaW8gPSBuZXcgQXVkaW8oKTtcbiAgICBhdWRpby52b2x1bWUgPSB2b2x1bWU7XG4gICAgYXVkaW8uc3JjID0gc3JjO1xuICAgIGF1ZGlvLmF1dG9wbGF5ID0gdHJ1ZTtcbiAgICByZXR1cm4gYXVkaW87XG4gIH07XG5cbiAgcmV0dXJuIFRveTtcblxufSkoKTtcblxuIl19
