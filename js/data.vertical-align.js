/*
 * Copyright (c) 2015
 * Data Scripts - Vertical Align
 * Version 1.0.2
 * Create 2015.12.02
 * Author Bunker Labs

 * Usage:
 *
 * Add class name 'Script-Vertical-Align' - to vertical align Element in Box
 * Add attribute  'data-shift'            - to set vertical shift

 * Code structure:
 * <div><img class="Script-Vertical-Align" src="..." alt="..."></div>
 */
(function ($) {
    $(document).ready(function () {
        Vertical_Align();
        $(window).resize(function () {
            Vertical_Align();
        });

        function Vertical_Align() {
            $('.Script-Vertical-Align').each(function () {
                var Element_Shift = $(this).attr('data-shift') ? $(this).attr('data-shift') : 0;

                $(this).css('margin-top',
                    ($(this).parent().actual('outerHeight') / 2) - ($(this).actual('outerHeight') / 2) + Element_Shift
                );
            });
        }
    });
})(jQuery);